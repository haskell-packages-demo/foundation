import Basement.From (from)

import Foundation
    ( -- (+) -- temporarily unused
      ($)
    -- , (>>=) -- temporarily unused
    -- , Integer -- temporarily unused
    , IO
    -- , putStrLn -- temporarily unused
    , String
    )

-- import Foundation.Conduit  -- temporarily unused
--     ( (.|)
    -- , mapC
--     , runConduitPure
--     , runConduitRes
--     , sinkList
--     , sinkFile
    -- , sourceFileBS
    -- , sumC
    -- , yieldMany
--     )

import Foundation.IO
    ( hPut
    , IOMode (WriteMode)
    -- , readFile -- temporarily unused
    , withFile
    )

import Foundation.VFS.FilePath (FilePath)

-----

-- | The computation 'writeFile' @file str@ function writes the string @str@,
-- to the file @file@.
writeFile :: FilePath -> String -> IO ()
writeFile f txt = withFile f WriteMode (\ hdl -> hPut hdl $ from txt)
-- from:
-- https://www.stackage.org/haddock/lts-19.28/base-4.15.1.0/src/System.IO.html#writeFile

main :: IO ()
main = do
    -- Pure operations: summing numbers.
    -- putStrLn $ show $ runConduitPure $ yieldMany [(1 :: Integer)..10] .| sumC

    -- Exception safe file access: copy a file.
    writeFile "input.txt" "This is a test." -- create the source file
    -- runConduitRes $ sourceFileBS "input.txt" .| sinkFile "output.txt" -- actual copying
    -- readFile "output.txt" >>= putStrLn -- prove that it worked

    -- Perform transformations.
    -- putStrLn $ show $ runConduitPure $ yieldMany [(1 :: Integer)..10] .| mapC (+ 1) .| sinkList

-- from:
-- https://github.com/snoyberg/conduit#readme
